#!/usr/bin/env python3
'''
covid19gis - Obtención de información sobre COVID-19 desde ArcGIS

Asignatura: Paradigmas de la Programación, LCIk (FP-UNA)
Autor: Prof. Carlos Zayas (czayas en gmail)
Fecha: 15/03/2020 (creación)

Para más información, visitar:
https://coronavirus-resources.esri.com/datasets/bbb2e4f589ba40d692fab712ae37b9ac_1/geoservice
'''

import urllib.request
import urllib.parse
import json
import os
import ssl
from datetime import datetime

def obtener(url):
    '''Obtiene del URL la información publicada en formato JSON.'''
    if (not os.environ.get('PYTHONHTTPSVERIFY', '') and
            getattr(ssl, '_create_unverified_context', None)):
        ssl._create_default_https_context = ssl._create_unverified_context
    HEADERS = {'User-Agent': 'Mozilla/5.0'}
    REQ = urllib.request.Request(url, headers=HEADERS)
    RESP = urllib.request.urlopen(REQ)
    RESPDATA = RESP.read().decode('utf-8')
    return json.loads(RESPDATA) # retorna una lista - <class 'list'>

def generar(dic={}):
    '''Genera el diccionario de países y lo guarda en un archivo JSON.'''
    with open("covid19gis.url") as archivo:
        url = archivo.readline().strip()
    datos = obtener(url)
    for item in datos['features']:
        pais = item['attributes']
        clave = pais['Country_Region'] if not pais['Province_State'] else \
                pais['Country_Region'] + ' - ' + pais['Province_State']
        actualizado = datetime.today() if not pais['Last_Update'] else \
                      datetime.fromtimestamp(pais['Last_Update']/1000)
        dic[clave] = {
            'Actualizado': actualizado.strftime("%d/%m/%Y, %H:%M:%S"),
            'Confirmados': pais['Confirmed'],
            'Recuperados': pais['Recovered'],
            'Muertes': pais['Deaths']}
    with open('covid19gis.json', 'w') as archivo:
        json.dump(dic, archivo, sort_keys=True, indent=4)
    return dic

def imprimir(dic):
    '''Imprime un diccionario.'''
    salida = ""
    claves = dic.keys()
    relleno = max(map(len, claves))
    for clave in claves:
        salida += "{} : {}\n".format(clave.ljust(relleno), dic[clave])
    return salida.strip()

class Main:
    '''Clase principal.'''

    def __init__(self):
        '''Constructor.'''
        self.dic = generar()
        print("Estado del Coronavirus en el Mundo")
        entrada = "X"
        while entrada:
            entrada = input("\nPaís: ").lower()
            if entrada:
                claves = sorted([clave for clave, valor in self.dic.items()
                                 if entrada in clave.lower()])
                for clave in claves:
                    print("\n{}\n{}".format(clave, imprimir(self.dic[clave])))

if __name__ == '__main__':
    Main()
