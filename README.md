# covid19

Este repositorio contiene scripts que realizan web scraping y consultas JSON sobre COVID-19, principalmente en Paraguay.

Estos scripts requieren de Python 3 con su biblioteca estándar de módulos (PSL). No utiliza módulos adicionales.

## covid19.py

Este script puede ejecutarse como programa principal o importarse desde otro script.

Al ejecutarse, imprime la cantidad de casos registrados de COVID-19 en el Paraguay, de acuerdo al dato suministrado en el [sitio web del MSPyBS](https://www.mspbs.gov.py/covid-19.php).

Ejemplo:

```
$ chmod +x covid19.py
$ ./covid19.py
Positivos: 133
Fallecidos: 6
Recuperados: 18
```

Al importarse, ofrece las siguientes funciones:

### navegar(url)
Retorna el contenido de la página referenciada.

```
pagina = navegar("https://www.mspbs.gov.py/covid-19.php")
```

### buscar(cadena, buscada, cantidad)
Retorna una cantidad determinada de caracteres que están a continuación de una buscada.

```
dato = buscar(archivo, "CONTADOR OFICIAL COVID-19 EN PARAGUAY: \r\n\t", 10)
```

### numeros(cadena)
Retorna una lista con los números encontrados en una cadena.

```
lista = numeros('Hay 133 positivos, 6 fallecidos y 18 recuperados.')
```

### casos()
Retorna una lista con los casos registrados de COVID-19 en Paraguay.

```
print("Casos registrados:", casos())
```

## covid19gis.py

Este script también puede ejecutarse como programa principal, o importarse desde otro script, como el anterior.

Al ejecutarse, obtiene la información compartida por ESRI en [ArcGIS sobre COVID-19](https://coronavirus-resources.esri.com/datasets/bbb2e4f589ba40d692fab712ae37b9ac_1/geoservice) a nivel mundial, genera un diccionario con dicha información resumida y la guarda en un archivo JSON con el nombre **covid19gis.json**.

Al importarse, ofrece la siguiente función:

### obtener(url)
Obtiene del URL la información publicada en formato JSON.

```
datos = obtener("https://jsonplaceholder.typicode.com/posts")
```
