#!/usr/bin/env python3
"""
covid19msp.py - Web scraping de la página con el contador oficial del COVID-19
                del Ministerio de Salud Pública y Bienestar Social del Paraguay.
                Copyleft 2020, Carlos Augusto Zayas Guggiari <czayas en gmail>
"""

import ssl
from urllib.request import urlopen

def navegar(url):
    """Retorna el contenido de la página referenciada."""
    contexto = ssl.create_default_context()
    contexto.check_hostname = False
    contexto.verify_mode = ssl.CERT_NONE
    with urlopen(url, context=contexto) as pagina:
        return pagina.read().decode("utf-8")

def buscar(cadena, buscada, cantidad):
    """Retorna una cantidad de caracteres que le siguen a la buscada."""
    posicion = cadena.find(buscada) + len(buscada)
    return cadena[posicion:posicion+cantidad]

def numeros(cadena):
    """Retorna una lista con los números encontrados en una cadena."""
    caracteres = list(cadena)
    lista = []
    numero = ''
    while caracteres:
        caracter = caracteres.pop(0)
        if caracter.isdigit():
            numero += caracter
        else:
            if numero:
                lista.append(numero)
                numero = ''
    return lista

def casos():
    """Retorna la cantidad de casos registrados de COVID-19 en Paraguay."""
    archivo = navegar("https://www.mspbs.gov.py/covid-19.php")
    buscada = 'CONTADOR COVID-19 PY:  <br>'
    cadena = buscar(archivo, buscada, 300)
    confirmados = buscar(cadena, 'Confirmados: <font color="#ffe500">', 40)
    fallecidos = buscar(cadena, 'Fallecidos: <font color="#ffe500">', 40)
    recuperados = buscar(cadena, 'Recuperados: <font color="#ffe500">', 40)
    return [numeros(i)[0] for i in (confirmados, fallecidos, recuperados)]

if __name__ == "__main__":
    print('Confirmados: {}\nFallecidos: {}\nRecuperados: {}'.format(*casos()))
